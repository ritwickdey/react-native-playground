const validation = (val, rules, connectedValue) => {
  let isValid = true;
  Object.keys(rules).forEach(ruleKey => {
    switch (ruleKey) {
      case 'isEmail':
        isValid = isValid && isEmail(val);
        break;
      case 'minLength':
        isValid = isValid && minLength(val, rules[ruleKey]);
        break;
      case 'maxLength':
        isValid = isValid && maxLength(val, rules[ruleKey]);
        break;
      case 'equalTo':
        isValid = isValid && isEqual(val, connectedValue[ruleKey]);
        break;
      case 'requried':
      case 'isRequried':
        isValid = isValid && !!(val || '').toString().trim();
        break;
      default:
        throw new Error(`${rules} is not supported.`);
    }
  });

  return isValid;
};

const minLength = (val = '', minLength) => val.toString().length >= minLength;

const maxLength = (val = '', maxLength) => val.toString().length <= maxLength;

const isEqual = (val, testVal) => val === testVal;

const isEmail = v =>
  RegExp(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/).test(v);

export default validation;
