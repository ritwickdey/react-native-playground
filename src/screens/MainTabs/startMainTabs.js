import { Navigation } from 'react-native-navigation';

import Icon from 'react-native-vector-icons/Ionicons';

const startMainTabs = async () => {
  Navigation.startTabBasedApp({
    tabs: [
      {
        label: 'Find Place',
        title: 'Find Place',
        screen: 'awesome-places.FindPlaceScreen',
        icon: await Icon.getImageSource('ios-navigate', 30),
        navigatorButtons: {
          leftButtons: [
            {
              icon: await Icon.getImageSource('md-menu', 30),
              title: 'Menu',
              id: 'sideBarToggle'
            }
          ]
        }
      },
      {
        label: 'Share Place',
        title: 'Share Place',
        screen: 'awesome-places.SharePlaceScreen',
        icon: await Icon.getImageSource('md-share', 30),
        navigatorButtons: {
          leftButtons: [
            {
              icon: await Icon.getImageSource('md-menu', 30),
              title: 'Menu',
              id: 'sideBarToggle'
            }
          ]
        }
      }
    ],
    tabsStyle: {
      tabBarSelectedButtonColor: '#0984e3'
    },
    appStyle: {
      tabBarSelectedButtonColor: '#0984e3'
    },
    drawer: {
      left: {
        screen: 'awesome-places.SideDrawerScreen'
      }
    }
  });
};

export default startMainTabs;
