import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableNativeFeedback,
  TouchableOpacity,
  Platform
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import TouchableCrossPlatform from '../../components/TouchableCrossPlatform/TouchableCrossPlatform';

class SideDrawer extends Component {
  render() {
    const content = '';
    return (
      <View
        style={[
          styles.drawerContainer,
          { width: Dimensions.get('window').width * 0.75 }
        ]}
      >
        <TouchableCrossPlatform>
          <View style={styles.drawerItem}>
            <Icon style={styles.drawerItemIcon} name="ios-log-out" size={25} />
            <Text style={styles.drawerItemText}>Log Out</Text>
          </View>
        </TouchableCrossPlatform>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  drawerContainer: {
    padding: 10,
    paddingTop: 20,
    backgroundColor: '#eee',
    flex: 1
  },
  drawerItem: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5
  },
  drawerItemIcon: {
    marginRight: 10
  },
  drawerItemText: {
    fontSize: 16
  }
});

export default SideDrawer;
