import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import MapView, { Marker } from 'react-native-maps';
import Icon from 'react-native-vector-icons/Ionicons';

import { startDeletePlace } from '../../store/actions';

class PlaceDetails extends Component {
  onCloseHandler = () => {
    this.props.navigator.pop({ animationType: 'slide-horizontal' });
  };

  onDeleteHandler = () => {
    this.props.deletePlace(this.props.place.key);
    this.onCloseHandler();
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <View>
          <Text style={styles.placeName}>{this.props.place.name}</Text>
          <Image
            style={styles.placeImage}
            resizeMode="stretch"
            source={this.props.place.image}
          />
          <MapView style={styles.map} region={this.props.place.location}>
            <Marker coordinate={this.props.place.location} />
          </MapView>
        </View>

        <View style={styles.buttonGroups}>
          <View style={styles.button}>
            <TouchableOpacity onPress={this.onDeleteHandler}>
              <Icon color="#d63031" name="md-trash" size={30} />
            </TouchableOpacity>
          </View>
          <View style={styles.button}>
            <TouchableOpacity onPress={this.onCloseHandler}>
              <Icon color="#0984e3" name="md-close" size={30} />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 20,
    display: 'flex',
    flexDirection: 'column'
  },
  buttonGroups: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    width: '100%',
    paddingTop: 8,
    paddingBottom: 8,
    backgroundColor: '#eee'
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '45%'
  },
  placeImage: {
    height: 200,
    borderWidth: 2,
    borderColor: '#555',
    width: '100%'
  },
  placeName: {
    fontSize: 40,
    marginBottom: 5
  },
  map: {
    height: 150,
    width: '100%',
    marginTop: 8
  }
});

const mapDispatchToProps = dispatch => ({
  deletePlace: key => dispatch(startDeletePlace(key))
});

export default connect(undefined, mapDispatchToProps)(PlaceDetails);
