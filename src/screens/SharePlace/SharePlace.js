import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  Button,
  StyleSheet,
  ActivityIndicator,
  KeyboardAvoidingView
} from 'react-native';
import { connect } from 'react-redux';

import { startAddPlace } from '../../store/actions';

import MyButton from '../../components/ui/MyButton/MyButton';
import MainText from '../../components/ui/MainText/MainText';
import Header from '../../components/ui/Header/Header';

import validator from '../../utils/validation';

import ImgPickerWithPreview from '../../components/ImgPickerWithPreview/ImgPickerWithPreview';
import LocationPicker from '../../components/LocationPicker/LocationPicker';
import PlaceInput from '../../components/PlaceInput/PlaceInput';

class SharePlaceScreen extends Component {
  static navigatorStyle = {
    navBarButtonColor: '#0984e3'
  };

  state = {
    controls: {
      placeName: {
        value: '',
        valid: false,
        touched: false,
        validationRules: {
          requried: true
        }
      },
      location: {
        valid: false,
        value: null
      },
      image: {
        valid: false,
        value: null
      }
    }
  };

  constructor(props) {
    super(props);
    props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  onNavigatorEvent = event => {
    if (event.type === 'NavBarButtonPress' && event.id === 'sideBarToggle') {
      this.props.navigator.toggleDrawer({
        side: 'left',
        animated: true,
        to: 'open'
      });
    }
  };

  onPlaceTextChange = value =>
    this.setState(oldState => ({
      controls: {
        ...oldState.controls,
        placeName: {
          ...oldState.controls.placeName,
          value,
          valid: validator(value, oldState.controls.placeName.validationRules)
        }
      }
    }));

  placeAddedHandler = () => {
    this.props.onAddPlaceSubmit(
      this.state.controls.placeName.value,
      this.state.controls.location.value,
      this.state.controls.image.value
    );
    this.onPlaceTextChange('');
  };

  locationSetHandler = location => {
    this.setState(oldState => ({
      controls: {
        ...oldState.controls,
        location: {
          ...oldState.controls.location,
          value: location,
          valid:
            typeof location === 'object' &&
            location.latitude &&
            location.longitude
        }
      }
    }));
  };

  picturePickedHandler = val => {
    this.setState(oldState => ({
      controls: {
        ...oldState.controls,
        image: {
          ...oldState.controls.image,
          value: val,
          valid: typeof val === 'object' && val.uri && val.base64
        }
      }
    }));
  };

  getValidityOfControls = () =>
    Object.keys(this.state.controls).reduce(
      (prev, key) => prev && this.state.controls[key].valid,
      true
    );

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <KeyboardAvoidingView
            keyboardVerticalOffset={-80}
            behavior="position"
          >
            <View style={{ marginBottom: 8 }}>
              <MainText>
                <Header>Share your favorite place!</Header>
              </MainText>
            </View>
            <ImgPickerWithPreview onPicturePicked={this.picturePickedHandler} />
            <LocationPicker onLocationPicked={this.locationSetHandler} />

            <PlaceInput
              style={styles.placeInput}
              placeName={this.state.controls.placeName.value}
              onChangeText={this.onPlaceTextChange}
            />
            <View style={styles.btnView}>
              {this.props.isLoading ? (
                <ActivityIndicator />
              ) : (
                <MyButton
                  disabled={!this.getValidityOfControls()}
                  onPress={this.placeAddedHandler}
                  title="Share the Place!"
                />
              )}
            </View>
          </KeyboardAvoidingView>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: '5%',
    flex: 1,
    alignItems: 'center',
    marginBottom: 20,
    marginTop: 5
  },
  btnView: {
    marginTop: 4,
    marginBottom: 4,
    flex: 1,
    justifyContent: 'center'
  },
  placeInput: {
    borderWidth: 1,
    borderColor: '#eee',
    padding: 8,
    fontSize: 14,
    backgroundColor: '#f2f2f2'
  }
});

const mapStateToProps = state => ({
  isLoading: state.ui.isLoading
});

const mapDispatchToProps = dispatch => ({
  onAddPlaceSubmit: (placeName, location, image) =>
    dispatch(startAddPlace(placeName, location, image))
});

export default connect(mapStateToProps, mapDispatchToProps)(SharePlaceScreen);
