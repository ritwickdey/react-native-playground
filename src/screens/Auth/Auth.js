import React, { Component } from 'react';
import {
  StyleSheet,
  ImageBackground,
  View,
  Dimensions,
  KeyboardAvoidingView,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

import startMainTabs from '../MainTabs/startMainTabs';
import Input from '../../components/ui/Input/Input';
import Header from '../../components/ui/Header/Header';
import MainText from '../../components/ui/MainText/MainText';
import RoundButton from '../../components/ui/RoundButton/RoundButton';

import validator from '../../utils/validation';
import { tryAuth, authAutoSiginIn } from '../../store/actions';

import BackgroundImage from '../../assets/background.jpg';

const getDimensions = () => Dimensions.get('window');

class AuthScreen extends Component {
  state = {
    window: getDimensions(),
    authMode: 'login',
    controls: {
      email: {
        value: '',
        valid: false,
        touched: false,
        validationRules: {
          isEmail: true,
          isRequried: true
        }
      },
      password: {
        value: '',
        valid: false,
        touched: false,
        validationRules: {
          minLength: 6,
          maxLength: 32,
          isRequried: true
        }
      },
      confirmPassword: {
        value: '',
        valid: false,
        touched: false,
        validationRules: {
          minLength: 6,
          maxLength: 32,
          isRequried: false,
          equalTo: 'password'
        }
      }
    }
  };

  componentDidMount() {
    this.props.onAutoSignIn();
    Dimensions.addEventListener('change', this.updateStateOnDimensionChange);
  }

  componentWillUnmount() {
    Dimensions.removeEventListener('change', this.updateStateOnDimensionChange);
  }

  updateStateOnDimensionChange = ({ window }) => {
    this.setState(() => ({ window }));
  };

  toggleAuthModeHandler = () => {
    this.setState(oldState => ({
      authMode: oldState.authMode === 'login' ? 'signup' : 'login',
      controls: {
        ...oldState.controls,
        confirmPassword: {
          ...oldState.controls.confirmPassword,
          validationRules: {
            ...oldState.controls.confirmPassword.validationRules,
            isRequried: oldState.authMode === 'login'
          }
        }
      }
    }));
  };

  getAuthText = () => (this.state.authMode === 'login' ? 'Login' : 'Signup');

  authHandler = () => {
    const controls = this.state.controls;
    this.props.tryAuth(
      {
        email: controls.email.value,
        password: controls.password.value
      },
      this.state.authMode
    );
  };

  getValidityOfControls = () =>
    Object.keys(this.state.controls).reduce(
      (prev, key) =>
        prev &&
        (this.state.controls[key].validationRules.isRequried === true
          ? this.state.controls[key].valid
          : true),
      true
    );

  onControlTextChange = (key, value) => {
    const connectedValue = {};
    if ('equalTo' in this.state.controls[key].validationRules) {
      const toEqualControl = this.state.controls[key].validationRules.equalTo;
      const toEqualControlValue = this.state.controls[toEqualControl].value;
      connectedValue['equalTo'] = toEqualControlValue;
    }

    this.setState(oldState => ({
      controls: {
        ...oldState.controls,
        [key]: {
          ...oldState.controls[key],
          touched: true,
          value,
          valid: validator(
            value,
            oldState.controls[key].validationRules,
            connectedValue
          )
        }
      }
    }));

    if (key === 'password') {
      connectedValue['equalTo'] = value;
      this.setState(oldState => ({
        controls: {
          ...oldState.controls,
          confirmPassword: {
            ...oldState.controls.confirmPassword,
            valid: validator(
              oldState.controls.confirmPassword.value,
              oldState.controls.confirmPassword.validationRules,
              connectedValue
            )
          }
        }
      }));
    }
  };

  render() {
    const headlingText =
      this.state.window.height > 500 ? (
        <MainText>
          <Header>{this.getAuthText()}</Header>
        </MainText>
      ) : null;

    const confirmPasswordControl =
      this.state.authMode === 'signup' ? (
        <Input
          value={this.state.controls.confirmPassword.value}
          onChangeText={val => this.onControlTextChange('confirmPassword', val)}
          secureTextEntry={true}
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Confirm Password"
          isInvalid={
            this.state.controls.confirmPassword.touched &&
            !this.state.controls.confirmPassword.valid
          }
        />
      ) : null;

    const buttonOrLoading = this.props.isLoading ? (
      <ActivityIndicator />
    ) : (
      <RoundButton
        disabled={!this.getValidityOfControls()}
        title={this.getAuthText()}
        onPress={this.authHandler}
      />
    );

    return (
      <ImageBackground
        source={BackgroundImage}
        blurRadius={2}
        style={style.backgroundImage}
      >
        <KeyboardAvoidingView style={style.conatiner} behavior="padding">
          <Icon name="ios-contact-outline" size={40} color="#bbb" />
          {headlingText}
          <View style={style.submitContainer}>
            <RoundButton
              title={`Switch to ${
                this.state.authMode === 'login' ? 'Signup' : 'Login'
              }`}
              onPress={this.toggleAuthModeHandler}
            />
          </View>
          <View style={style.inputContainer}>
            <Input
              value={this.state.controls.email.value}
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="email-address"
              onChangeText={val => this.onControlTextChange('email', val)}
              placeholder="Email"
              isInvalid={
                this.state.controls.email.touched &&
                !this.state.controls.email.valid
              }
            />
            <Input
              value={this.state.controls.password.value}
              secureTextEntry={true}
              autoCorrect={false}
              autoCapitalize="none"
              onChangeText={val => this.onControlTextChange('password', val)}
              placeholder="Password"
              isInvalid={
                this.state.controls.password.touched &&
                !this.state.controls.password.valid
              }
            />
            {confirmPasswordControl}
          </View>
          <View style={style.submitContainer}>
            {buttonOrLoading}
            {/* <RoundButton title="Skip Login" onPress={startMainTabs} /> */}
          </View>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

const style = StyleSheet.create({
  conatiner: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },
  inputContainer: {
    width: '85%',
    marginTop: 15,
    marginBottom: 15
  },
  submitContainer: {
    marginTop: 15
  },
  backgroundImage: {
    width: '100%',
    flex: 1
  }
});

const mapStateToProps = state => ({
  isLoading: state.ui.isLoading
});

const mapDispathToProps = dispatch => ({
  tryAuth: (authData, authMode) => dispatch(tryAuth(authData, authMode)),
  onAutoSignIn: () => dispatch(authAutoSiginIn())
});

export default connect(mapStateToProps, mapDispathToProps)(AuthScreen);
