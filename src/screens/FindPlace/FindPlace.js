import React, { Component } from 'react';
import { View, Text, StyleSheet, Animated } from 'react-native';
import { connect } from 'react-redux';

import PlaceList from '../../components/PlaceList/PlaceList';
import RoundButton from '../../components/ui/RoundButton/RoundButton';

import { startSetPlaces } from '../../store/actions';

class FindPlaceScreen extends Component {
  static navigatorStyle = {
    navBarButtonColor: '#0984e3'
  };

  constructor(props) {
    super(props);
    props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  state = {
    isLoaded: false,
    removeAnimate: new Animated.Value(1)
  };

  componentDidMount() {
    this.props.fetchPlacesData();
  }

  onNavigatorEvent = event => {
    if (event.type === 'NavBarButtonPress' && event.id === 'sideBarToggle') {
      this.props.navigator.toggleDrawer({
        side: 'left',
        animated: true,
        to: 'open'
      });
    }
  };

  placeSearchHandler = () => {
    Animated.timing(this.state.removeAnimate, {
      toValue: 0,
      duration: 500,
      useNativeDriver: true
    }).start(() => {
      this.setState(() => ({
        isLoaded: true
      }));
    });
  };

  onItemSeletedHandler = key => {
    const place = this.props.places.find(place => place.key === key);
    this.props.navigator.push({
      screen: 'awesome-places.PlaceDetailScreen',
      title: `Place - ${place.name}`,
      animationType: 'slide-horizontal',
      passProps: { place }
    });
  };

  render() {
    const searchBtnJsx = (
      <Animated.View
        style={{
          opacity: this.state.removeAnimate,
          transform: [
            {
              scale: this.state.removeAnimate.interpolate({
                inputRange: [0, 1],
                outputRange: [10, 1]
              })
            }
          ]
        }}
      >
        <RoundButton
          onPress={this.placeSearchHandler}
          textStyle={styles.findPlaceButtonTxt}
          style={styles.findPlaceButton}
          title="Find Places"
        />
      </Animated.View>
    );

    return (
      <View style={styles.container}>
        {!this.state.isLoaded ? (
          searchBtnJsx
        ) : (
          <PlaceList
            places={this.props.places}
            onItemSelected={this.onItemSeletedHandler}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  findPlaceButton: {
    borderWidth: 2,
    backgroundColor: '#fff',
    borderColor: '#0984e3',
    width: '90%'
  },
  findPlaceButtonTxt: {
    color: '#0984e3',
    fontSize: 24
  }
});

const mapStateToProps = state => ({
  places: state.places.places
});

const mapDispatchToProps = dispatch => ({
  fetchPlacesData: () => dispatch(startSetPlaces())
});

export default connect(mapStateToProps, mapDispatchToProps)(FindPlaceScreen);
