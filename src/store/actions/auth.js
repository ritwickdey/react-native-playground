import { AsyncStorage } from 'react-native';
import { TRY_AUTH, SET_AUTH_TOKEN } from './actionTypes';
import { uiStopLoading, uiStartLoading } from './';

import startMainTabs from '../../screens/MainTabs/startMainTabs';

const API_KEY = 'AIzaSyCBJlL3Qexd91uH03D0rCfYxTWC_VN-GZo';
const AUTH_URL = `https://www.googleapis.com/identitytoolkit/v3/relyingparty`;
const GET_AUTH_URL = endPoint => `${AUTH_URL}/${endPoint}?key=${API_KEY}`;

const STORAGE_KEY_TOKEN = 'awesomeplaces:auth:token';
const STORAGE_KEY_REFRESH_TOKEN = 'awesomeplaces:auth:refreshToken';
const STORAGE_KEY_EXPIREDBY = 'awesomeplaces:auth:expiredBy';

export const tryAuth = (authData, authMode) => dispatch => {
  return dispatch(authLoginOrSignUp(authData, authMode));
};

const authLoginOrSignUp = (authData, authMode) => {
  return dispatch => {
    dispatch(uiStartLoading());
    return fetch(
      GET_AUTH_URL(authMode == 'login' ? 'verifyPassword' : 'signupNewUser'),
      {
        method: 'POST',
        body: JSON.stringify({
          email: authData.email,
          password: authData.password,
          returnSecureToken: true
        }),
        headers: { 'Content-Type': 'application/json' }
      }
    )
      .then(res => res.json())
      .then(data => {
        if (data.error) data.error.errors.forEach(e => alert(e.message));
        else if (!data.idToken) alert('Something Went Wrong!');
        else {
          dispatch(
            authStoreToken({
              token: data.idToken,
              refreshToken: data.refreshToken,
              expiresIn: data.expiresIn
            })
          );
          startMainTabs();
        }
      })
      .catch(err => console.log(err))
      .then(() => dispatch(uiStopLoading()));
  };
};

export const getAuthToken = () => {
  return (dispatch, getState) => {
    return new Promise((resolve, reject) => {
      const token = getState().auth.token;
      if (token) return resolve(token);
      let storedToken;
      return AsyncStorage.getItem(STORAGE_KEY_TOKEN)
        .then(tokenFromStorage => {
          storedToken = tokenFromStorage;
          return AsyncStorage.getItem(STORAGE_KEY_EXPIREDBY);
        })
        .then(expiresBy => {
          if (storedToken && expiresBy && +expiresBy > Date.now()) {
            dispatch(setAuthToken(storedToken));
            return resolve(storedToken);
          }
          reject('Token Not Found Or Invalid');
        });
    }).catch(err => {
      dispatch(authClearStorage(storedToken));
      throw new Error(err);
    });
  };
};

const setAuthToken = token => ({
  type: SET_AUTH_TOKEN,
  token
});

const authStoreToken = ({ token, refreshToken, expiresIn = 0 }) => {
  return dispatch => {
    dispatch(setAuthToken(token));
    AsyncStorage.setItem(STORAGE_KEY_TOKEN, token);
    AsyncStorage.setItem(
      STORAGE_KEY_EXPIREDBY,
      (Date.now() + expiresIn * 1000).toString()
    );
  };
};

const authClearStorage = () => dispatch =>
  AsyncStorage.multiRemove([
    STORAGE_KEY_TOKEN,
    STORAGE_KEY_REFRESH_TOKEN,
    STORAGE_KEY_EXPIREDBY
  ]);

export const authAutoSiginIn = () => dispatch => {
  dispatch(getAuthToken())
    .then(token => {
      if (token) startMainTabs();
    })
    .catch(err => console.log('No Token found! Auto signin failed', err));
};
