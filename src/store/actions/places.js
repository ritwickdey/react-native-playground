import { ADD_PLACE, DELETE_PLACE, SET_PLACES } from './actionTypes';
import { getRandomKey } from '../../common';

import { uiStartLoading, uiStopLoading, getAuthToken } from './';

const URL = 'https://ritwick-awesome-places.firebaseio.com';
const IMAGE_STORE_URL =
  'https://us-central1-ritwick-awesome-places.cloudfunctions.net/storeImage';

export const addPlace = (key, placeName, location, image) => ({
  type: ADD_PLACE,
  place: {
    key,
    name: placeName,
    location: location,
    image: { uri: image.uri }
  }
});

export const startAddPlace = (name, location, image) => {
  const placeData = { name, location, image: { uri: null } };
  let idToken;
  return dispatch => {
    dispatch(uiStartLoading());
    dispatch(getAuthToken())
      .then(token => {
        idToken = token;
        return fetch(IMAGE_STORE_URL + '?auth=' + idToken, {
          method: 'POST',
          body: JSON.stringify({ image: image.base64 })
        });
      })
      .then(res => res.json())
      .then(({ imageUrl }) => {
        placeData.image.uri = imageUrl;
        return fetch(URL + '/places.json?auth=' + idToken, {
          method: 'POST',
          body: JSON.stringify(placeData)
        });
      })
      .then(res => res.json())
      .then(({ name: key }) =>
        dispatch(addPlace(key, name, location, placeData.image))
      )
      .catch(err => console.log(err) || alert('Failed to add data'))
      .then(() => dispatch(uiStopLoading()));
  };
};

export const setPlaces = places => ({
  type: SET_PLACES,
  places
});

export const startSetPlaces = () => {
  return dispatch => {
    dispatch(getAuthToken())
      .then(token => fetch(URL + '/places.json?auth=' + token))
      .then(res => res.json())
      .then(parsedRes =>
        Object.keys(parsedRes || []).map(key => ({ key, ...parsedRes[key] }))
      )
      .then(places => dispatch(setPlaces(places)))
      .catch(err => alert('Error to fetch data') || console.log(err));
  };
};

export const deletePlace = key => ({
  type: DELETE_PLACE,
  placeKey: key
});

export const startDeletePlace = key => {
  return dispatch => {
    return dispatch(getAuthToken())
      .then(token =>
        fetch(URL + `/places/${key}.json?auth=${token}`, {
          method: 'DELETE'
        })
      )
      .then(res => res.json())
      .then(() => dispatch(deletePlace(key)))
      .catch(err => alert('Failed to delete') || console.log(err));
  };
};
