import { ADD_PLACE, DELETE_PLACE, SET_PLACES } from '../actions/actionTypes';
import { getRandomKey } from '../../common';
import placeImage from '../../assets/img.jpg';

const initState = {
  places: [
    // { key: getRandomKey(), name: 'Kolkata', image: placeImage },
    // { key: getRandomKey(), name: 'Delhi', image: placeImage },
    // { key: getRandomKey(), name: 'Mumbai', image: placeImage },
    // { key: getRandomKey(), name: 'Pune', image: placeImage },
    // { key: getRandomKey(), name: 'Bangalore', image: placeImage }
  ]
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case ADD_PLACE:
      return {
        ...state,
        places: [...state.places, action.place]
      };
    case SET_PLACES:
      console.log('setPlace');
      return {
        ...state,
        places: [...action.places]
      };
    case DELETE_PLACE:
      return {
        ...state,
        places: state.places.filter(e => e.key !== action.placeKey)
      };
    default:
      return state;
  }
};

export default reducer;
