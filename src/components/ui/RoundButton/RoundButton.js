import React from 'react';
import { StyleSheet } from 'react-native';

import MyButton from '../MyButton/MyButton';

const RoundButton = props => <MyButton {...props} style={[styles.RoundButton, props.style]} textStyle={[styles.textStyle, props.textStyle]} />;

const styles = StyleSheet.create({
  RoundButton: {
    borderRadius: 100,
    padding: 40,
    paddingTop: 10,
    paddingBottom: 10
  },
  textStyle: {
      fontSize: 14
  }
});

export default RoundButton;
