import React from 'react';
import { StyleSheet, TextInput } from 'react-native';

const Input = props => (
  <TextInput
    underlineColorAndroid="transparent"
    {...props}
    style={[style.input, props.style, props.isInvalid ? style.invalid : null]}
  />
);

const style = StyleSheet.create({
  input: {
    width: '100%',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: 'rgba(248,248,248, 0.7)',
    marginTop: 8,
    marginBottom: 8,
    padding: 5,
    backgroundColor: 'rgba(248,248,248, 0.5)'
  },
  invalid: {
    //backgroundColor: '#d63031',
    borderColor: '#d63031',
    borderWidth: 2
  }
});

export default Input;
