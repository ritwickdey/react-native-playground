import React from 'react';
import { StyleSheet, View, Text, Platform } from 'react-native';

import TouchableCrossPlatform from '../../TouchableCrossPlatform/TouchableCrossPlatform';

const MyButton = props => {
  return (
    <TouchableCrossPlatform
      onPress={e => {
        !props.disabled && props.onPress(e);
      }}
    >
      <View
        {...props}
        style={[
          styles.button,
          { backgroundColor: props.color || '#0984e3' },
          props.style,
          props.disabled
            ? props.style && props.style.disabled
              ? props.style.disabled
              : styles.disabled
            : null
        ]}
      >
        <Text
          style={[
            { color: props.textColor || '#fff' },
            ,
            props.textStyle,
            props.disabled
              ? props.style && props.style.disabledText
                ? props.style.disabledText
                : styles.disabledText
              : null
          ]}
        >
          {props.title}
        </Text>
      </View>
    </TouchableCrossPlatform>
  );
};

const styles = StyleSheet.create({
  button: {
    padding: 8,
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  disabled: {
    backgroundColor: 'rgba(9, 132, 227, 0.5)'
  },
  disabledText: {
    color: '#eee'
  }
});

export default MyButton;
