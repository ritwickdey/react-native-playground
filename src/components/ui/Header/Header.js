import React from 'react';
import { StyleSheet, Text } from 'react-native';

const Header = props => (
  <Text {...props} style={[styles.text, props.style]}>
    {props.children}
  </Text>
);

const styles = StyleSheet.create({
  text: {
    fontSize: 28,
    fontWeight: 'bold'
  }
});

export default Header;
