import React, { Component } from 'react';

import Input from '../ui/Input/Input';

const PlaceInput = props => (
  <Input
    style={props.style}
    placeholder="Place Name"
    onChangeText={props.onChangeText}
    value={props.placeName}
  />
);

export default PlaceInput;
