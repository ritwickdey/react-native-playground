import React from 'react';
import {
  TouchableNativeFeedback,
  TouchableOpacity,
  Platform
} from 'react-native';

const TouchableCrossPlatform = props => {
  return Platform.OS === 'android' ? (
    <TouchableNativeFeedback {...props}>
      {props.children}
    </TouchableNativeFeedback>
  ) : (
    <TouchableOpacity {...props}>{props.children}</TouchableOpacity>
  );
};

export default TouchableCrossPlatform;
