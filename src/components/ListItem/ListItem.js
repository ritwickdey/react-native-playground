import React from 'react';
import {
  StyleSheet,
  Image,
  View,
  Text,
  TouchableNativeFeedback
} from 'react-native';

const ListItem = props => (
  <TouchableNativeFeedback onPress={props.onItemPress}>
    <View style={styles.listItem}>
      <Image
        resizeMode="cover"
        style={styles.image}
        source={props.place.image}
      />
      <Text>{props.place.name}</Text>
    </View>
  </TouchableNativeFeedback>
);

const styles = StyleSheet.create({
  listItem: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    marginBottom: 5,
    padding: 10,
    backgroundColor: '#eee'
  },
  image: {
    height: 35,
    width: 35,
    marginRight: 10
  }
});

export default ListItem;
