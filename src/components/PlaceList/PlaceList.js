import React from 'react';
import { StyleSheet, FlatList, View, Text } from 'react-native';

import ListItem from '../ListItem/ListItem';

const PlaceList = props => {
  return (
    <FlatList
      style={styles.placeListContainer}
      data={props.places}
      renderItem={({ item: place }) => (
        <ListItem
          place={place}
          onItemPress={() => props.onItemSelected(place.key)}
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  placeListContainer: { width: '100%' }
});

export default PlaceList;
