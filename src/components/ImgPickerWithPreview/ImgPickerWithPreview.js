import React, { Component } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import ImagePicker from 'react-native-image-picker';

import MyButton from '../ui/MyButton/MyButton';

// import SampleImage from '../../assets/img.jpg';

class ImgPickerWithPreview extends Component {
  state = {
    pickedImage: null
  };

  pickImageHandler = () => {
    ImagePicker.showImagePicker(
      {
        title: 'Pick an Image'
      },
      res => {
        if (res.didCancel) {
          console.log('User Cancled');
        } else if (res.error) {
          console.log('Error', res.error);
        } else {
          const pickedImage = { uri: res.uri, base64: res.data };
          this.setState(() => ({ pickedImage }));
          this.props.onPicturePicked(pickedImage);
        }
      }
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.placeholder}>
          <Image
            resizeMode="stretch"
            source={this.state.pickedImage}
            style={styles.previewImg}
          />
        </View>
        <View style={styles.btnView}>
          <MyButton onPress={this.pickImageHandler} title="Pick Image" />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center'
  },
  btnView: {
    margin: 8
  },
  placeholder: {
    borderWidth: 1,
    borderColor: '#555',
    backgroundColor: '#eee',
    width: '100%',
    height: 150
  },
  previewImg: {
    height: '100%',
    width: '100%'
  }
});

export default ImgPickerWithPreview;
