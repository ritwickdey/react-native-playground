import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, Image } from 'react-native';
import MapView, { Marker } from 'react-native-maps';

import MyButton from '../ui/MyButton/MyButton';

import SampleImage from '../../assets/img.jpg';

class LocationPicker extends Component {
  state = {
    region: {
      latitude: 37.78825,
      longitude: -122.4324,
      latitudeDelta: 0.0122,
      longitudeDelta:
        Dimensions.get('window').width /
        Dimensions.get('window').height *
        0.0121
    },
    locationChoose: false
  };

  selectLocationHandler = e => {
    const coordinate = e.nativeEvent.coordinate;
    console.log(coordinate);
    this.setState(prevState => ({
      region: {
        ...prevState.region,
        latitude: coordinate.latitude,
        longitude: coordinate.longitude
      },
      locationChoose: true
    }));

    this.map.animateToRegion({
      ...this.state.region,
      latitude: coordinate.latitude,
      longitude: coordinate.longitude
    });

    this.props.onLocationPicked({
      ...this.state.region,
      latitude: coordinate.latitude,
      longitude: coordinate.longitude
    });
  };

  getLocationHandler = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.selectLocationHandler({
          nativeEvent: {
            coordinate: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude
            }
          }
        });
      },
      err => {
        console.log(err);
        alert('Unable to locate you. Please try manually');
      }
    );
  };

  render() {
    const marker = this.state.locationChoose ? (
      <Marker coordinate={this.state.region} />
    ) : null;

    return (
      <View style={styles.container}>
        <MapView
          ref={ref => (this.map = ref)}
          style={styles.map}
          initialRegion={this.state.region}
          onPress={this.selectLocationHandler}
        >
          {marker}
        </MapView>

        <View style={{ marginTop: 8 }}>
          <MyButton title="Locate Me" onPress={this.getLocationHandler} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center'
  },
  btnView: {
    margin: 8
  },
  map: {
    height: 200,
    width: '100%'
  }
});

export default LocationPicker;
