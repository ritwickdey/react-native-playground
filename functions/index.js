const fs = require('fs');
const uuidv4 = require('uuid/v4');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const cors = require('cors')({ origin: true });
const gcs = require('@google-cloud/storage')({
  projectId: 'ritwick-awesome-places',
  keyFilename: '.firebaseSecretKey.json'
});

admin.initializeApp({
  credential: admin.credential.cert(require('./.firebaseSecretKey.json'))
});

exports.storeImage = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    const token = req.query['auth'];
    console.log('Token: ' + token);
    if (!token) return res.status(403).json({ error: 'Token is not provided' });
    admin
      .auth()
      .verifyIdToken(token)
      .then(verifiedToken => {
        const body = JSON.parse(req.body);
        try {
          fs.writeFileSync('/tmp/uploaded-image.jpg', body.image, 'base64');
          const bucket = gcs.bucket('ritwick-awesome-places.appspot.com');
          const uuid = uuidv4();
          bucket
            .upload('/tmp/uploaded-image.jpg', {
              uploadType: 'media',
              destination: `/places/${uuid}.jpg`,
              metadata: {
                metadata: {
                  contentType: 'image/jpeg',
                  firebaseStorageDownloadTokens: uuid
                }
              }
            })
            .then(files => {
              const file = files['0'];
              console.log('file', file.name);
              return res.status(201).json({
                imageUrl: `https://firebasestorage.googleapis.com/v0/b/${
                  bucket.name
                }/o/${encodeURIComponent(file.name)}?alt=media&token=${uuid}`
              });
            })
            .catch(error => {
              console.log('error1', error);
              res.status(500).json({ error });
            });
        } catch (error) {
          console.log('error2', error);
          return res.status(500).json({ error });
        }
      })
      .catch(err => {
        console.log('Invalid Token. Error: ', err);
        res.status(403).json({ error: 'Invalid Token' });
      });
  });
});
